﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumericKeyboardController : MonoBehaviour, IClickableMenu {
    public ButtonController btn0;
    public ButtonController btn1;
    public ButtonController btn2;
    public ButtonController btn3;
    public ButtonController btn4;
    public ButtonController btn5;
    public ButtonController btn6;
    public ButtonController btn7;
    public ButtonController btn8;
    public ButtonController btn9;

    public ButtonController btnDone;
    public ButtonController btnBackspace;
    public ButtonController btnCancel;

    public ButtonController input;

    public SettingsController settings;

    public void ClickAction (string GOName) {
        if (btn0.GetName () == GOName) {
            input.SetText ($"{input.GetText()}0");
        } else if (btn1.GetName () == GOName) {
            input.SetText ($"{input.GetText()}1");
        } else if (btn2.GetName () == GOName) {
            input.SetText ($"{input.GetText()}2");
        } else if (btn3.GetName () == GOName) {
            input.SetText ($"{input.GetText()}3");
        } else if (btn4.GetName () == GOName) {
            input.SetText ($"{input.GetText()}4");
        } else if (btn5.GetName () == GOName) {
            input.SetText ($"{input.GetText()}5");
        } else if (btn6.GetName () == GOName) {
            input.SetText ($"{input.GetText()}6");
        } else if (btn7.GetName () == GOName) {
            input.SetText ($"{input.GetText()}7");
        } else if (btn8.GetName () == GOName) {
            input.SetText ($"{input.GetText()}8");
        } else if (btn9.GetName () == GOName) {
            input.SetText ($"{input.GetText()}9");
        } else if (btnBackspace.GetName () == GOName) {
            string inputStr = input.GetText ();
            int l = inputStr.Length - 1;
            if (l >= 0) {
                inputStr = inputStr.Substring (0, l);
                input.SetText (inputStr);
            }
        } else if (btnCancel.GetName () == GOName) {
            gameObject.SetActive (false);
            settings.SetActive (true);
        } else if (btnDone.GetName () == GOName) {
            string inputStr = input.GetText ();
            gameObject.SetActive (false);
            settings.SetActive (true);
            settings.UpdateField (inputStr);
        }
    }

    public void SetActive (bool act) {
        gameObject.SetActive (act);
    }
}