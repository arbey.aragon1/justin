﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour, IClickableMenu, IKeyboard {

    private enum StatusUpdate { ip0, ip1, ip2, ip3, port, none }
    private StatusUpdate status = StatusUpdate.none;

    public ButtonController ip0;
    public ButtonController ip1;
    public ButtonController ip2;
    public ButtonController ip3;
    public ButtonController port;

    public ButtonController save;
    public ButtonController cancel;

    public GameObject mainMenu;

    public NumericKeyboardController numericKeyboard;

    void Start () {
        GameStatus gameStatus = GameObject.Find ("GameStatus").GetComponent<GameStatus> ();
        this.port.SetText (gameStatus.GetPort ());
        string[] splitArray = gameStatus.GetIP ().Split (char.Parse ("."));
        this.ip0.SetText (splitArray[0]);
        this.ip1.SetText (splitArray[1]);
        this.ip2.SetText (splitArray[2]);
        this.ip3.SetText (splitArray[3]);
    }

    public void ClickAction (string GOName) {
        if (save.GetName () == GOName) {
            GameStatus gameStatus = GameObject.Find ("GameStatus").GetComponent<GameStatus> ();
            gameStatus.SetIP ($"{this.ip0.GetText()}.{this.ip1.GetText()}.{this.ip2.GetText()}.{this.ip3.GetText()}");
            gameStatus.SetPort ($"{this.port.GetText ()}");

            gameObject.SetActive (false);
            mainMenu.SetActive (true);
            this.status = StatusUpdate.none;
        } else if (cancel.GetName () == GOName) {
            gameObject.SetActive (false);
            mainMenu.SetActive (true);
            this.status = StatusUpdate.none;
        } else if (ip0.GetName () == GOName) {
            gameObject.SetActive (false);
            numericKeyboard.SetActive (true);
            this.status = StatusUpdate.ip0;
        } else if (ip1.GetName () == GOName) {
            gameObject.SetActive (false);
            numericKeyboard.SetActive (true);
            this.status = StatusUpdate.ip1;
        } else if (ip2.GetName () == GOName) {
            gameObject.SetActive (false);
            numericKeyboard.SetActive (true);
            this.status = StatusUpdate.ip2;
        } else if (ip3.GetName () == GOName) {
            gameObject.SetActive (false);
            numericKeyboard.SetActive (true);
            this.status = StatusUpdate.ip3;
        } else if (port.GetName () == GOName) {
            gameObject.SetActive (false);
            numericKeyboard.SetActive (true);
            this.status = StatusUpdate.port;
        }
    }

    public void SetActive (bool act) {
        gameObject.SetActive (act);
    }

    public void UpdateField (string value) {
        if (this.status == StatusUpdate.ip0) {
            this.ip0.SetText (value);
        } else if (this.status == StatusUpdate.ip1) {
            this.ip1.SetText (value);
        } else if (this.status == StatusUpdate.ip2) {
            this.ip2.SetText (value);
        } else if (this.status == StatusUpdate.ip3) {
            this.ip3.SetText (value);
        } else if (this.status == StatusUpdate.port) {
            this.port.SetText (value);
        }
        this.status = StatusUpdate.none;
    }

}