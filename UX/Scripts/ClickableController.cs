﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableController : MonoBehaviour {
    public ButtonController buttonController;
    private Material m_Material;

    void Start () {
        m_Material = GetComponent<Renderer> ().material;
    }

    public Material GetMaterial () {
        return m_Material;
    }

    public void SetMaterial (Material m) {
        GetComponent<Renderer> ().material = m;
    }

    public void ResetMaterial () {
        GetComponent<Renderer> ().material = m_Material;
    }

    public void ClickAction () {
        Debug.Log ("*** 2 ***");

        buttonController.ClickAction ();
    }
}