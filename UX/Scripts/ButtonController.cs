﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonController : MonoBehaviour {
    public GameObject gomenu;
    public TMPro.TextMeshProUGUI gotext;

    public void SetText (string value) {
        if (gotext != null) {
            gotext.GetComponent<TMPro.TextMeshProUGUI> ().text = value;
        }
    }

    public string GetText () {
        string val = "";
        if (gotext != null) {
            val = gotext.GetComponent<TMPro.TextMeshProUGUI> ().text;
        }
        return val;
    }

    public string GetName () {
        return gameObject.name;
    }

    public void ClickAction () {
        Debug.Log ("*** 3 ***");

        var menu = gomenu.GetComponent<IClickableMenu> ();
        if (menu != null) {
            Debug.Log ("*** 4 ***");
            menu.ClickAction (gameObject.name);
        }
    }
}