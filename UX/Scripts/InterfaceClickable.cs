﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClickableMenu {
    void ClickAction (string GOName);
}