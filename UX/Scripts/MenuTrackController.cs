﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTrackController : MonoBehaviour {
    private GameObject _camera;
    private const float _distance = 1.5f;

    void Start () {
        _camera = GameObject.Find ("Main Camera");
    }

    void Update () {
        HeadLock (gameObject, 5.0f);
        //HeadLock(gameObject, 5.0f);
    }

    public void HeadLock (GameObject obj, float speed) {
        speed = Time.deltaTime * speed;
        Vector3 posTo = _camera.transform.position + (_camera.transform.forward * _distance);
        obj.transform.position = Vector3.SlerpUnclamped (obj.transform.position, posTo, speed);
        Quaternion rotTo = Quaternion.LookRotation (obj.transform.position - _camera.transform.position);
        obj.transform.rotation = Quaternion.Slerp (obj.transform.rotation, rotTo, speed);
    }
}