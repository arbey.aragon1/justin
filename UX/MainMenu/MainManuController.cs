﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainManuController : MonoBehaviour, IClickableMenu {
    public ButtonController buttonStartGame;
    public ButtonController buttonSettings;
    public ButtonController buttonExit;

    public GameObject settingsMenu;

    void IClickableMenu.ClickAction (string GOName) {
        if (buttonStartGame.GetName () == GOName) {
            SceneManager.LoadScene ("SpaceConfig", LoadSceneMode.Single);
        } else if (buttonSettings.GetName () == GOName) {
            gameObject.SetActive (false);
            settingsMenu.SetActive (true);
        } else if (buttonExit.GetName () == GOName) {
            Application.Quit ();
        }
    }
}