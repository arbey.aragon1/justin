using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class GameController : MonoBehaviour
{
    public DialogController dialogController;
    public TMPro.TextMeshProUGUI labelOnline;
    public FirebaseController firebaseController;
    public AudioController audioController;
    
    public Image circularSpinner;
    private bool release = true;
    private float countTime = 0.0f;
    public TMPro.TextMeshProUGUI textSpinner;
    private bool spinnerActive = false;
    private float timeRecord = 10f;
    
    
    void Awake()
    {
        firebaseController.Login((string x) => {
            this.SetOnlineText("+Online");
            dialogController.loadPlayerDataFromWeb();
            StartCoroutine(StartConversation(2f));
        });
        circularSpinner.gameObject.SetActive(false);
        textSpinner.gameObject.SetActive(false);
    }

    
    private IEnumerator StartConversation(float time)
    {
        yield return new WaitForSeconds (time);
        this.dialogController.LoadNextAudioClip();
        yield return new WaitForSeconds (time);
        Next();
    }

    private IEnumerator NextJustin(float time)
    {
        yield return new WaitForSeconds (time);
        Next();
    }

    void Update()
    {
        if(this.spinnerActive){
            this.countTime = this.countTime + Time.deltaTime;
            circularSpinner.fillAmount = this.countTime/timeRecord;
            SetSpinnerText((Convert.ToInt32(100*circularSpinner.fillAmount)).ToString()+"%");
        }
    }

    
    private void SetSpinnerText(string text){
        textSpinner.GetComponent<TMPro.TextMeshProUGUI> ().text = text;
    }

    public void Next(){
        if(release){
            release = false;
            circularSpinner.gameObject.SetActive(true);
            textSpinner.gameObject.SetActive(true);
            this.spinnerActive = true;
            this.countTime = 0.0f;
            this.audioController.RecordUser(timeRecord, (string x) => {
                circularSpinner.gameObject.SetActive(false);
                textSpinner.gameObject.SetActive(false);
                this.spinnerActive = false;
                this.dialogController.LoadNextAudioClip();
                release = true;
                StartCoroutine(NextJustin(8f));
            });
        }
    }

    private void UpdateOnlineOfline(){
    }
    
    
    private void SetOnlineText(string text){
        labelOnline.GetComponent<TMPro.TextMeshProUGUI> ().text = text;
    }

    public void ChangeLanguage()
    {
        dialogController.ChangeLanguage();
    }
}
