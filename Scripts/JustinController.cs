using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;

public class JustinController : MonoBehaviour
{
    public enum Status { Neutral, Surprise, Happy, Disgust, Fear, Angry, Sad }
    private static Dictionary<string, Status> StatusDic = new Dictionary<string, Status> { { "neutral", Status.Neutral },
        { "surprise", Status.Surprise },
        { "happy", Status.Happy },
        { "disgust", Status.Disgust },
        { "fear", Status.Fear },
        { "angry", Status.Angry },
        { "sad", Status.Sad },
    };

    //private float startAnim;
    //private float endAnim;
    //private Status status;

    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public static Status GetStatusValue(string value)
    {
        Debug.Log(value);
        return StatusDic[value];
    }

    public void RunEmotions(List<Emotion> emotions){
        foreach(Emotion em in emotions)
        {
            Animation a = em.animation;
            this.UpdateStatus(a.start, a.end, em.emotion);
        }
    }

    public void UpdateStatus(float startAnim, float endAnim, string st)
    {
        Status status = JustinController.GetStatusValue(st);

        StartCoroutine(StartAnim(startAnim, endAnim, status));
        StartCoroutine(EndAnim(endAnim));
    }

    IEnumerator StartAnim(float startAnim, float endAnim, Status status)
    {
        yield return new WaitForSeconds(startAnim / 1000f);
        anim.SetInteger("status", 0);
        if (status == Status.Neutral)
        {
            anim.SetInteger("status", 0);
        }
        else if (status == Status.Surprise)
        {
            anim.SetInteger("status", 1);
        }
        else if (status == Status.Happy)
        {
            anim.SetInteger("status", 2);
        }
        else if (status == Status.Disgust)
        {
            anim.SetInteger("status", 3);
        }
        else if (status == Status.Fear)
        {
            anim.SetInteger("status", 4);
        }
        else if (status == Status.Angry)
        {
            anim.SetInteger("status", 5);
        }
        else if (status == Status.Sad)
        {
            anim.SetInteger("status", 6);
        }
    }

    IEnumerator EndAnim(float endAnim)
    {
        yield return new WaitForSeconds(endAnim / 1000f);
        anim.SetInteger("status", 0);
    }
}