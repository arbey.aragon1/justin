using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;

public class DialogController : MonoBehaviour
{
    private string _pathMenuIndex;
    
    public TMPro.TextMeshProUGUI labelLanguage;

    private AnimationStatus animationStatusData;

    private DialogFlowMock _dialogFlow;
    private ConfigurationModel configurationModel;
    private Configuration configuration;

    private Dictionary<int, AnimationStatus> _dialogDic;
    private Dictionary<int, string> languages;
    private int lagunageId = 0;

    public AudioController audioController;

    public TMPro.TextMeshProUGUI labelJustinText;

    public JustinController justinController;

    private bool offline = true;

    public int currentId = -1;

    public FirebaseController firebaseController;
    public string datetime;

    void Awake()
    {
        /*this._pathMenuIndex = Application.dataPath + "/StreamingAssets/dialog.json";

        if (File.Exists(this._pathMenuIndex))
        {
            
            string dataAsJson = File.ReadAllText(this._pathMenuIndex);
            Debug.Log(dataAsJson);

            this._dialogFlow =
                JsonUtility.FromJson<DialogFlowMock>(dataAsJson);
            this._dialogDic = this._dialogFlow.GenerateDictionary();
        }
        else
        {
            throw new Exception("File in : " + this._pathMenuIndex + " not exist");
        }/**/
        //StartCoroutine(loadPlayerData());
        //loadPlayerDataFromWeb();
        languages = new Dictionary<int, string>();
        languages.Add(0, "fr");
        languages.Add(1, "en");

        
        DateTime theTime = DateTime.Now;
        this.datetime = theTime.ToString("yyyy-MM-dd-HH-mm");
    }

    public void loadPlayerDataFromWeb()
    {

        firebaseController.GetConfiguration((string x0) => {
            this.configurationModel = JsonUtility.FromJson<ConfigurationModel>(x0);
            this.configuration = this.configurationModel.GenerateDictionary();
            SetLanguageText(this.configuration.language);

            firebaseController.GetAllDialogs((string x1) => {
                this._dialogFlow = JsonUtility.FromJson<DialogFlowMock>("{\"dialog\":"+x1+"}");
                this._dialogDic = this._dialogFlow.GenerateDictionary();
            });
        });


    }

    private IEnumerator loadPlayerData()
    {
        string filePath1 =
            Path.Combine(Application.streamingAssetsPath, "dialog.json");
        string filePath2 =
            Path.Combine(Application.persistentDataPath, "dialog.json");

        //string filePath3 = Path.Combine(Application.dataPath, "dialog.json");
        string path = filePath2;
        if (File.Exists(path))
        {
            UnityWebRequest www = UnityWebRequest.Get(path);
            yield return www.SendWebRequest();
            string jsonString = www.downloadHandler.text;

            //File.WriteAllText(path, jsonString);
            if (www.isNetworkError)
            {
                Debug.Log("Error: " + www.error);
            }
            else
            {
                Debug.Log("Received: " + jsonString);

                string dataAsJson = jsonString;

                this._dialogFlow =
                    JsonUtility.FromJson<DialogFlowMock>(dataAsJson);
                this._dialogDic = this._dialogFlow.GenerateDictionary();
            }
        }
    }

    public void UpdateOnlineOfline()
    {
        this.offline = !this.offline;
    }

    public void LoadNextAudioClip()
    {
        this.currentId = (this.currentId + 1) % this._dialogDic.Count;
        LoadAudioClip();
    }

    public void LoadAudioClip()
    {
        this.animationStatusData = this._dialogDic[this.currentId];
        this.audioController
            .LoadAudioClipFromFolder(this.configuration.language,this.animationStatusData.audioURL);
        this.SetJustinText(this.configuration.language,this.animationStatusData);
        this.justinController.RunEmotions(this.animationStatusData.emotions);

        //JustinController.Status st = JustinController.GetStatusValue();
        //this.justinController.UpdateStatus(float startAnim, float endAnim, Status st)
    }

    private void SetJustinText(string language, AnimationStatus animationStatusData)
    {
        if(language == "fr"){
        labelJustinText.GetComponent<TMPro.TextMeshProUGUI>().text =
            $"Justin: {animationStatusData.textFr}";
        } else if(language == "en"){
        labelJustinText.GetComponent<TMPro.TextMeshProUGUI>().text =
            $"Justin: {animationStatusData.textEn}";
        }
    }
    
    public void SetLanguageText(string text){
        labelLanguage.GetComponent<TMPro.TextMeshProUGUI> ().text = text;
    }

    public void ChangeLanguage()
    {
        this.lagunageId = (this.lagunageId + 1) % this.languages.Count;
        this.configuration.language = this.languages[this.lagunageId];
        SetLanguageText(this.configuration.language);
        firebaseController.UpdateConfig(this.configuration, (string x)=>{});
    }
}
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
public class Animation
{
    public float start { get; set; }

    public float end { get; set; }

    public Animation()
    {
    }

    public Animation(JSONObject animation)
    {
        this.start =
            float.Parse(animation["start"].ToString().Replace("\"", ""));
        this.end = float.Parse(animation["end"].ToString().Replace("\"", ""));
    }

    public override string ToString()
    {
        return "\n start: " + this.start + "\n end: " + this.end;
    }
}

public class Emotion
{
    public string emotion { get; set; }

    public Animation animation { get; set; }

    public Emotion(JSONObject animation)
    {
        this.emotion = animation["emotion"].ToString().Replace("\"", "");
        this.animation = new Animation(animation["animation"]);
    }

    public Emotion()
    {
    }

    public override string ToString()
    {
        return "\n emotion: " +
        this.emotion +
        "\n animation: " +
        this.animation.ToString();
    }
}

public class Fire
{
    public float start { get; set; }

    public float end { get; set; }

    public Fire(JSONObject fire)
    {
        this.start = float.Parse(fire["start"].ToString().Replace("\"", ""));
        this.end = float.Parse(fire["end"].ToString().Replace("\"", ""));
    }

    public Fire()
    {
    }

    public override string ToString()
    {
        return "\n start: " + this.start + "\n end: " + this.end;
    }
}

public class Animations
{
    public Fire fire { get; set; }

    public Animations(JSONObject animations)
    {
        this.fire = new Fire(animations["fire"]);
    }

    public Animations()
    {
    }

    public override string ToString()
    {
        return "\n fire: " + this.fire.ToString();
    }
}

public class World
{
    public Animations animations { get; set; }

    public World(JSONObject world)
    {
        this.animations = new Animations(world["animations"]);
    }

    public World()
    {
    }

    public override string ToString()
    {
        return "\n animations: " + this.animations.ToString();
    }
}

public class AnimationStatus
{
    public AudioClip clip { get; set; }

    public int id { get; set; }

    public string textEn { get; set; }
    public string textFr { get; set; }

    public float startSound { get; set; }

    public List<Emotion> emotions { get; set; }

    public string audioURL { get; set; }

    public World world { get; set; }

    public AnimationStatus()
    {
    }

    public void SetData(JSONObject data)
    {
        this.id = int.Parse(data["id"].ToString().Replace("\"", ""));
        this.textEn = data["textEn"].ToString().Replace("\"", "");
        this.textFr = data["textFr"].ToString().Replace("\"", "");
        this.startSound =
            float.Parse(data["startSound"].ToString().Replace("\"", ""));
        this.audioURL = data["audioURL"].ToString().Replace("\"", "");

        this.world = new World(data["world"]);

        bool valid = true;
        this.emotions = new List<Emotion>();
        for (int i = 0; valid; i = i + 1)
        {
            try
            {
                this.emotions.Add(new Emotion(data["emotions"][i]));
            }
            catch (Exception err)
            {
                valid = false;
            }
        }
    }

    public void SetAudioClip(AudioClip clip)
    {
        this.clip = clip;
    }

    public override string ToString()
    {
        return "id: " +
        this.id +
        "\n textEn: " +
        this.textEn +
        "\n textFr: " +
        this.textFr +
        "\n startSound: " +
        this.startSound +
        "\n audioURL: " +
        this.audioURL +
        "\n world: " +
        this.world.ToString() +
        "\n emotions: " +
        this.emotions.ToString();
    }
}

[Serializable]
public class AnimationMock
{
    public float start;

    public float end;

    public Animation Generate()
    {
        Animation a = new Animation();
        a.start = this.start;
        a.end = this.end;
        return a;
    }
}

[Serializable]
public class EmotionMock
{
    public string emotion;

    public AnimationMock animation;

    public Emotion Generate()
    {
        Emotion e = new Emotion();
        e.emotion = this.emotion;
        e.animation = this.animation.Generate();
        return e;
    }
}

[Serializable]
public class FireMock
{
    public float start;

    public float end;

    public Fire Generate()
    {
        Fire f = new Fire();
        f.start = this.start;
        f.end = this.end;
        return f;
    }
}

[Serializable]
public class AnimationsMock
{
    public FireMock fire;

    public Animations Generate()
    {
        Animations a = new Animations();
        a.fire = fire.Generate();
        return a;
    }
}

[Serializable]
public class WorldMock
{
    public AnimationsMock animations;

    public World Generate()
    {
        World w = new World();
        w.animations = this.animations.Generate();
        return w;
    }
}

[Serializable]
public class DialogMock
{
    public int id;

    public string textEn;
    public string textFr;

    public int startSound;

    public List<EmotionMock> emotions;

    public string audioURL;

    public WorldMock world;

    public AnimationStatus Generate()
    {
        AnimationStatus a = new AnimationStatus();
        a.id = this.id;
        a.textEn = this.textEn;
        a.textFr = this.textFr;
        a.startSound = this.startSound;
        a.audioURL = this.audioURL;
        a.world = world.Generate();

        List<Emotion> el = new List<Emotion>();
        foreach (EmotionMock m in this.emotions)
        {
            el.Add(m.Generate());
        }
        a.emotions = el;
        return a;
    }
}

[Serializable]
public class DialogFlowMock
{
    public List<DialogMock> dialog;

    public Dictionary<int, AnimationStatus> GenerateDictionary()
    {
        Dictionary<int, AnimationStatus> el =
            new Dictionary<int, AnimationStatus>();
        foreach (DialogMock m in this.dialog)
        {
            el.Add(m.id, m.Generate());
        }
        return el;
    }
}


public class Configuration
{
    public string language { get; set; }

    public Configuration(JSONObject configuration)
    {
        this.language = configuration["language"].ToString().Replace("\"", "");
    }
    public Configuration()
    {
    }

    public override string ToString()
    {
        return "\n language: " + this.language.ToString();
    }
}

[Serializable]
public class ConfigurationModel
{
    public string language;

    public Configuration GenerateDictionary()
    {
        Configuration f = new Configuration();
        f.language = this.language;
        return f;
    }
}

