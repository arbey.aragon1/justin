﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using SocketIO;
//using UniRx;
//using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Networking;

//[RequireComponent (typeof (SocketIOController))]
public class NetworkController : MonoBehaviour
{
    public AudioController audioController;
    private string _pathMenuIndex;
    private AnimationStatus animationStatusData;
    private DialogFlowMock _dialogFlow;
    private Dictionary<int, AnimationStatus> _dialogDic;
    
    private int _currentId = 0;

    void Awake()
    {
        this._pathMenuIndex = Application.dataPath + "/StreamingAssets/dialog.json";

        if (File.Exists(this._pathMenuIndex))
        {
            string dataAsJson = File.ReadAllText(this._pathMenuIndex);
            Debug.Log(dataAsJson);
            this._dialogFlow =
                JsonUtility.FromJson<DialogFlowMock>(dataAsJson);
            this._dialogDic = this._dialogFlow.GenerateDictionary();
        }
        else
        {
            throw new Exception("File in : " + this._pathMenuIndex + " not exist");
        }
    }

    public void UploadAudioClip(AudioClip audio)
    {
        this._currentId = (this._currentId + 1) % this._dialogDic.Count;
        OnChatCallback(this._currentId);
    }

    private void OnChatCallback(int id)
    {
        /*animationStatusData.SetData (args.data);
        Debug.Log (animationStatusData.ToString ());

        Debug.Log ("Load audio 0");
        StartCoroutine (GetAudioClip ());/**/
        animationStatusData = this._dialogDic[id];
        //StartCoroutine(LoadAudioClip());
        //LoadAudioClip ();
    }


    /*private void LoadAudioClip () {

        string path = Application.dataPath + $"/StreamingAssets/audio{this._currentId}.wav";
        Debug.Log ("LOADING CLIP " + path);
        WAV wav = new WAV (path);
        Debug.Log (wav);
        AudioClip audioClip = AudioClip.Create ("testSound", wav.SampleCount, 1, wav.Frequency, false, false);
        audioClip.SetData (wav.LeftChannel, 0);
        
        audioController.SetAudioClip(audioClip);
        
        //GetComponent<AudioSource> ().clip = audioClip;
        //GetComponent<AudioSource> ().Play ();

        //animationStatusData.SetAudioClip (audioClip);
        //_subject.OnNext (animationStatusData);

        //yield return new WaitForSeconds (1.5f);
    }/**/

    /*
    private BehaviorSubject<AnimationStatus> _subject;
    private string ip = "localhost";
    private AnimationStatus animationStatusData = new AnimationStatus ();

    public IObservable<AnimationStatus> FetchAudioClip () {
        if (_subject == null)
            _subject = new BehaviorSubject<AnimationStatus> (null);
        return _subject
            .AsObservable ();
    }

    private SocketIOController _socketComponent;
    void Start () {
        if (_subject == null)
            _subject = new BehaviorSubject<AnimationStatus> (null);
    }

    public void SetIp (string ip) {
        this.ip = ip;
    }

    public void Connect () {
        GetComponent<SocketIOController> ().url = "ws://" + ip + ":3000/socket.io/?EIO=4&transport=websocket";
        GetComponent<SocketIOController> ().autoConnect = true;
        Debug.Log ("ws://" + ip + ":3000/socket.io/?EIO=4&transport=websocket");
        _socketComponent = GetComponent<SocketIOController> ();
        _socketComponent.Connect ();
        _socketComponent.On ("chat", OnChatCallback);
        StartCoroutine (SendFistMessage ());
    }

    private void OnDestroy () {
        Close ();
    }

    public void Close () {
        _socketComponent.Close ();
    }

    public IEnumerator SendFistMessage () {
        yield return new WaitForSeconds (2.5f);
        var data = new Dictionary<string, string> ();
        data.Add ("uid", "client-unity");
        data.Add ("msg", "WELCOME");
        _socketComponent.Emit ("chat", new JSONObject (data));
        Debug.Log ("WELCOME");
    }

    private void OnChatCallback (SocketIOEvent args) {

        animationStatusData.SetData (args.data);

        Debug.Log (animationStatusData.ToString ());

        Debug.Log ("Load audio 0");
        StartCoroutine (GetAudioClip ());
    }

    private IEnumerator GetAudioClip () {
        Debug.Log ("Load audio 1");
        Debug.Log ("http://" + ip + ":3000/download");

        using (UnityWebRequest www =
            UnityWebRequestMultimedia.GetAudioClip ("http://" + ip + ":3000/audio-download", AudioType.WAV)) {
            yield return www.Send ();

            if (www.isNetworkError) {
                Debug.LogError (www.error);
            } else {
                Debug.Log ("Load audio 2");
                AudioClip clip = DownloadHandlerAudioClip.GetContent (www);
                animationStatusData.SetAudioClip (clip);
                _subject.OnNext (animationStatusData);
            }
        }
    }

    public void UploadAudioClip (AudioClip audio) {

        string path = SavWav.Save ("myfile", audio);
        path = path.Replace (@"\", "/");

        StartCoroutine (Upload (path, "myfile.wav"));
    }

    private IEnumerator Upload (string localPath, string name) {

        byte[] bytes = File.ReadAllBytes (localPath);

        List<IMultipartFormSection> formData = new List<IMultipartFormSection> ();
        formData.Add (new MultipartFormFileSection ("uploadFile", bytes, name, "audio/wav"));

        UnityWebRequest www = UnityWebRequest.Post ("http://" + ip + ":3000/audio-upload", formData);
        yield return www.SendWebRequest ();

        if (www.isNetworkError || www.isHttpError) {
            Debug.Log (www.error);
        } else {
            Debug.Log ("Form upload complete!");
        }
    }

}
/**/
}
/// //////////////////////////////////////////
