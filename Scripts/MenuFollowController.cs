﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFollowController : MonoBehaviour {
    public GameObject _camera;
    private const float _distance = 1.5f;
    private bool _fixWindow = false;

    void Start () {
    }

    void Update () {
        if(this._fixWindow){
            HeadLock (gameObject, 5.0f);
        }
    }

    public void HeadLock (GameObject obj, float speed) {
        speed = Time.deltaTime * speed;
        Quaternion rotTo = Quaternion.LookRotation (- _camera.transform.position + obj.transform.position);
        obj.transform.rotation = Quaternion.Slerp (obj.transform.rotation, rotTo, speed);
        obj.transform.position = _camera.transform.position + _camera.transform.forward * _distance;
    }

    public void FixWindow(){
        this._fixWindow = !this._fixWindow;
    }
}