﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public static class Utilities {
    public static string CreatePath (string filename) {
        if (!filename.ToLower ().EndsWith (".mp3")) {
            filename += ".mp3";
        }
        return Path.Combine (Application.persistentDataPath, filename);
    }

    public static string StopSoundReproduction = "StopSoundReproduction";
    public static string StopAudioRecording = "StopAudioRecording";
}