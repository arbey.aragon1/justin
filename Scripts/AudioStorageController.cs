using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStorageController : MonoBehaviour
{
    public AudioClip audioClip0;
    public AudioClip audioClip1;
    public AudioClip audioClip2;
    public AudioClip audioClip3;
    public AudioClip audioClip4;
    public AudioClip audioClip5;
    public AudioClip audioClip6;
    public AudioClip audioClip7;
    public AudioClip audioClip8;
    public AudioClip audioClip9;
    public AudioClip audioClip10;
    public AudioClip audioClip11;
    public AudioClip audioClip12;
    public AudioClip audioClip13;
    public AudioClip audioClip14;
    public AudioClip audioClip15;


    
    public AudioClip audioClipFr0;
    public AudioClip audioClipFr1;
    public AudioClip audioClipFr2;
    public AudioClip audioClipFr3;
    public AudioClip audioClipFr4;
    public AudioClip audioClipFr5;
    public AudioClip audioClipFr6;
    public AudioClip audioClipFr7;
    public AudioClip audioClipFr8;
    public AudioClip audioClipFr9;
    public AudioClip audioClipFr10;
    public AudioClip audioClipFr11;
    public AudioClip audioClipFr12;
    public AudioClip audioClipFr13;
    public AudioClip audioClipFr14;
    public AudioClip audioClipFr15;

    public AudioClip AudioClip(string laguage, string key){
        if(laguage == "en"){
            if(key ==       "audio0.wav"){ return audioClip0; } 
            else if(key ==  "audio1.wav"){ return audioClip1; }
            else if(key ==  "audio2.wav"){ return audioClip2; }
            else if(key ==  "audio3.wav"){ return audioClip3; }
            else if(key ==  "audio4.wav"){ return audioClip4; }
            else if(key ==  "audio5.wav"){ return audioClip5; }
            else if(key ==  "audio6.wav"){ return audioClip6; }
            else if(key ==  "audio7.wav"){ return audioClip7; }
            else if(key ==  "audio8.wav"){ return audioClip8; }
            else if(key ==  "audio9.wav"){ return audioClip9; }
            else if(key == "audio10.wav"){ return audioClip10; }
            else if(key == "audio11.wav"){ return audioClip11; }
            else if(key == "audio12.wav"){ return audioClip12; }
            else if(key == "audio13.wav"){ return audioClip13; }
            else if(key == "audio14.wav"){ return audioClip14; }
            else if(key == "audio15.wav"){ return audioClip15; }
        } else if(laguage == "fr"){
            if(key ==       "audio0.wav"){ return audioClipFr0; } 
            else if(key ==  "audio1.wav"){ return audioClipFr1; }
            else if(key ==  "audio2.wav"){ return audioClipFr2; }
            else if(key ==  "audio3.wav"){ return audioClipFr3; }
            else if(key ==  "audio4.wav"){ return audioClipFr4; }
            else if(key ==  "audio5.wav"){ return audioClipFr5; }
            else if(key ==  "audio6.wav"){ return audioClipFr6; }
            else if(key ==  "audio7.wav"){ return audioClipFr7; }
            else if(key ==  "audio8.wav"){ return audioClipFr8; }
            else if(key ==  "audio9.wav"){ return audioClipFr9; }
            else if(key == "audio10.wav"){ return audioClipFr10; }
            else if(key == "audio11.wav"){ return audioClipFr11; }
            else if(key == "audio12.wav"){ return audioClipFr12; }
            else if(key == "audio13.wav"){ return audioClipFr13; }
            else if(key == "audio14.wav"){ return audioClipFr14; }
            else if(key == "audio15.wav"){ return audioClipFr15; }
        }
        return null;
    }
}
