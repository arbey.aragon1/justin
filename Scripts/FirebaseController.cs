using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;

public class FirebaseController : MonoBehaviour
{
    private string apiKey = "AIzaSyDTGBtFFooX64ZqVsOCOXpfmSsVBEW5ghE";
    private string mainURL = "https://identitytoolkit.googleapis.com/v1/";
    private string databaseURL = "https://personalehr-c4616.firebaseio.com/";
    private string idToken;
    private string refreshToken;
    private string localId;
    private string key = "-Mdgi6VsN0p0AJT8qmYE";

    private bool logged = false;
    


    public void CreateUser(){
        string requestBodyJsonString = "{'email':'arbey.aragon@gmail.com','password':'0123456789', 'returnSecureToken':true}";
        string method = "POST";
        string url = $"{this.mainURL}accounts:signUp?key={this.apiKey}";
        Action<String> callback = (string x) => Debug.Log(x);
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback));
    }

    public void Login(Action<string> callback){
        this.logged = true;
        string requestBodyJsonString = "{'email':'arbey.aragon@gmail.com','password':'0123456789', 'returnSecureToken':true}";
        string method = "POST";
        string url = $"{this.mainURL}accounts:signInWithPassword?key={this.apiKey}";
        Action<string> callback1 = (string x) => {
            LoginResponse obj =
                JsonUtility.FromJson<LoginResponse>(x);
            idToken = obj.idToken;
            refreshToken = obj.refreshToken;
            localId = obj.localId;
            callback("");
        };
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback1));
    }

    public void SendMessage(){
        string path = "in-msg";
        string requestBodyJsonString = "{\"user_id\" : \"jack\", \"text\" : \"Ahoy!\"}";
        string method = "POST";
        string url = $"{this.databaseURL}{path}.json?auth={this.idToken}";
        
        Action<string> callback = (string x) => {
            MsgSendedResponse obj =
                JsonUtility.FromJson<MsgSendedResponse>(x);
            this.key = obj.name;
        };
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback));
    }

    public void ReadResponse(){
        string path = $"out-msg/{this.localId}/{this.key}";
        string method = "GET";
        string url = $"{this.databaseURL}{path}.json?auth={this.idToken}";
        
        Action<string> callback = (string x) => {
            Debug.Log(x);
            DialogMock obj =
                JsonUtility.FromJson<DialogMock>(x);
        };
        string requestBodyJsonString = "";
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback));
    }

    public void GetAllDialogs(Action<string> callback){
        string path = $"dialog";
        string method = "GET";
        string url = $"{this.databaseURL}{path}.json?auth={this.idToken}";
        
        Action<string> callback1 = (string x) => {
            callback(x);
        };
        string requestBodyJsonString = "";
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback1));
    }

    public void GetConfiguration(Action<string> callback){
        string path = $"configuration";
        string method = "GET";
        string url = $"{this.databaseURL}{path}.json?auth={this.idToken}";
        
        Action<string> callback1 = (string x) => {
            callback(x);
        };
        string requestBodyJsonString = "";
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback1));
    }

    public void UpdateConfig(Configuration conf, Action<string> callback){
        string path = $"configuration";
        string method = "PUT";
        string url = $"{this.databaseURL}{path}.json?auth={this.idToken}";
        
        Action<string> callback1 = (string x) => {
            callback(x);
        };
        string requestBodyJsonString = "{\"language\" : \""+conf.language+"\"}";
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method, callback1));
    }

    IEnumerator CreateRequest(string url, string requestBodyJsonString, string method, Action<string> callback)
    {
        UnityWebRequest request;
        
        switch (method)
        {
            case "POST":
            case "PATCH":
            // Defaults are fine for PUT
            case "PUT":
                byte[] bytes = new System.Text.UTF8Encoding().GetBytes(requestBodyJsonString);
                request = UnityWebRequest.Put(url, bytes);
                request.SetRequestHeader("X-HTTP-Method-Override", method);
                request.SetRequestHeader("accept", "application/json; charset=UTF-8");
                request.SetRequestHeader("content-type", "application/json; charset=UTF-8");
                break;
            case "GET":
                // Defaults are fine for GET
                request = UnityWebRequest.Get(url);
                break;
            case "DELETE":
                // Defaults are fine for DELETE
                request = UnityWebRequest.Delete(url);
                break;
            default:
                throw new Exception("Invalid HTTP Method");
        }

        yield return request.SendWebRequest();

        if (request.isNetworkError)
        {
            Debug.Log("Error While Sending: " + request.error);
        }
        else
        {
            callback(request.downloadHandler.text);
        }
    }

    
    public IEnumerator Upload (string localPath, string name, string folder, Action<string> callback) {

        byte[] bytes = File.ReadAllBytes (localPath);
        List<IMultipartFormSection> formData = new List<IMultipartFormSection> ();
        formData.Add (new MultipartFormFileSection ("uploadFile", bytes, name, "audio/mp3"));

        string buked = "personalehr-c4616.appspot.com";
        string url = $"https://firebasestorage.googleapis.com/v0/b/{buked}/o/{folder}%2F{name}";
        UnityWebRequest request = UnityWebRequest.Post(url, formData);

        request.SetRequestHeader("content-type", "audio/mp3");
        request.SetRequestHeader("authorization", "Bearer "+this.idToken);

        yield return request.SendWebRequest ();

        if (request.isNetworkError || request.isHttpError) {
            Debug.Log (request.error);
        } else {
            Debug.Log ("Form upload complete!");
        }
        callback("");
    }
}

[Serializable]
public class LoginResponse
{
    public string idToken;
    public string email;
    public string refreshToken;
    public string expiresIn;
    public string localId;
    public bool registered; 
}


[Serializable]
public class MsgSendedResponse
{
    public string name;
}