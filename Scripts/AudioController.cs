using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour
{
    public AudioStorageController audioStorageController;
    public FirebaseController firebaseController;
    public DialogController dialogController;
    private AudioSource _audioSource;
    private string _path;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void SetAudioClip(AudioClip clip)
    {
        _audioSource.clip = clip;
        _audioSource.pitch = 1f;
    }

    public AudioClip GetAudioClip()
    {
        return _audioSource.clip;
    }

    public void Play()
    {
        StartCoroutine(PlaySound());
    }

    private IEnumerator PlaySound()
    {
        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        _audioSource.Stop();
    }

    public void LoadAudioClipFromFolder(string laguage, string audioName)
    {
        /*
        string path = Application.dataPath + $"/StreamingAssets/{audioName}";
        Debug.Log("LOADING CLIP " + path);

        WAV wav = new WAV(path);
        AudioClip audioClip = AudioClip.Create("testSound", wav.SampleCount, 1, wav.Frequency, false, false);
        audioClip.SetData(wav.LeftChannel, 0);/**/
        AudioClip audioClip = this.audioStorageController.AudioClip(laguage, audioName);
        GetComponent<AudioSource>().clip = audioClip;
        GetComponent<AudioSource> ().Play ();

    }

    
    public void RecordUser(float time, Action<string> callback){
        StartCoroutine (_RecordUser (time, callback));
    }
    
    private IEnumerator _RecordUser(float time, Action<string> callback){
        this.StartRecord ();
        yield return new WaitForSeconds (time);
        this.StopRecord ();
        /*_audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        _audioSource.Stop();/**/
        //Save ();
        callback("");
    }

    public void StartRecord () {
        _audioSource.clip = Microphone.Start (
            Microphone.devices[0].ToString (), true, 10, 48000);
        _audioSource.pitch = 1f;
    }

    public void StopRecord () {
        Microphone.End (Microphone.devices[0].ToString ());
        _audioSource.pitch = 1f;
    }

    public void StopRecordStepTime (float time) {
        StartCoroutine (StopRecordInTime (time));
    }

    private IEnumerator StopRecordInTime (float time) {
        yield return new WaitForSeconds (time);
        this.StopRecord ();
        Debug.Log ("Stop record");
        //Play ();
    }

    public void Save () {
        string name = "audio_"+dialogController.currentId;
        this._path = Utilities.CreatePath (name);
        EncodeMP3.convert (_audioSource.clip, this._path, 128);
        this._path = this._path.Replace (@"\", "/");
        Debug.Log("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        Debug.Log(this._path);
        /*StartCoroutine (firebaseController.Upload (this._path, name+".mp3", dialogController.datetime, (string x)=>{
            if(File.Exists( this._path )){
                File.Delete (this._path);
                RefreshEditorProjectWindow() ;
            }
        }));/**/
    }

    void RefreshEditorProjectWindow() 
     {
         #if UNITY_EDITOR
         UnityEditor.AssetDatabase.Refresh();
         #endif
     }
}