﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour {
    private static GameStatus _instance;

    private string _ip = "192.168.1.102";
    private string _port = "3000";
    private Vector3 _positionWorld = new Vector3 (0, 0, 0);
    private Quaternion _rotatopnWorld = new Quaternion ();

    void Awake () {
        if (_instance == null) {
            _instance = this;
            DontDestroyOnLoad (gameObject);
        } else {
            Destroy (this);
        }
    }

    public void SetIP (string ip) {
        this._ip = ip;
    }

    public string GetIP () {
        return this._ip;
    }

    public void SetPort (string port) {
        this._port = port;
    }

    public string GetPort () {
        return this._port;
    }

    public void SetPositionWorld (Vector3 vec) {
        this._positionWorld = vec;
    }

    public Vector3 GetPositionWorld () {
        return this._positionWorld;
    }

    public void SetRotationWorld (Quaternion q) {
        this._rotatopnWorld = q;
    }

    public Quaternion GetRotationWorld () {
        return this._rotatopnWorld;
    }

}